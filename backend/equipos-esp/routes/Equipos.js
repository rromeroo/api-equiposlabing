const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('./cors');
const Equipos = require('../models/Equipos');

const EquiposRouter = express.Router();
EquiposRouter.use(bodyParser.json());

EquiposRouter.route('/').
options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Equipos.find({}).sort({Nombre: -1})
            .then((equipos) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(equipos);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions,
        (req, res, next) => {
        Equipos.create(req.body)
            .then((equipos) => {
                console.log('Equipo Created', equipos);
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(equipos);

            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .put(cors.corsWithOptions, 
        (req, res, next) => {
        res.statusCode = 403;
        res.end('La operacion PUT no es soportada para esta ruta');
    })
    .delete(cors.corsWithOptions, 
        (req, res, next) => {
        res.statusCode = 403;
        res.end('La operacion DELETE no es soportada para esta ruta');
    });

EquiposRouter.route('/:equipoId').
options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Equipos.findById(req.params.equipoId)
            .then((equipos) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(equipos);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions, 
        (req, res, next) => {

    })
    .put(cors.corsWithOptions, 
        (req, res, next) => {
        Equipos.findByIdAndUpdate(req.params.equipoId, {
            $set: req.body
        }, { new: true })
            .then((equipos) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(equipos);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .delete(cors.corsWithOptions, 
        (req, res, next) => {
        Equipos.findByIdAndRemove(req.params.equipoId)
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });



module.exports = EquiposRouter;
