const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('./cors');
const Activities = require('../models/Actividad');

const ActivityRouter = express.Router();
ActivityRouter.use(bodyParser.json());

ActivityRouter.route('/').
    options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Activities.find({}).sort({ Nombre: -1 })
            .then((activities) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(activities);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions,
        (req, res, next) => {
            Activities.create(req.body)
                .then((activity) => {
                    console.log('Activity Created', activity);
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(activity);

                }, (err) => next(err))
                .catch((err) => next(err));
        })
    .put(cors.corsWithOptions,
        (req, res, next) => {
            res.statusCode = 403;
            res.end('La operacion PUT no es soportada para esta ruta');
        })
    .delete(cors.corsWithOptions,
        (req, res, next) => {
            res.statusCode = 403;
            res.end('La operacion DELETE no es soportada para esta ruta');
        });

ActivityRouter.route('/:activityId').
    options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    .get(cors.cors, (req, res, next) => {
        Activities.findById(req.params.activityId)
            .then((activities) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(activities);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    .post(cors.corsWithOptions,
        (req, res, next) => {
            res.statusCode = 403;
            res.end('La operacion DELETE no es soportada para esta ruta');

        })
    .put(cors.corsWithOptions,
        (req, res, next) => {
            Activities.findByIdAndUpdate(req.params.activityId, {
                $set: req.body
            }, { new: true })
                .then((activity) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(activity);
                }, (err) => next(err))
                .catch((err) => next(err));
        })
    .delete(cors.corsWithOptions,
        (req, res, next) => {
            Activities.findByIdAndRemove(req.params.activityId)
                .then((resp) => {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(resp);
                }, (err) => next(err))
                .catch((err) => next(err));
        });


module.exports = ActivityRouter;
