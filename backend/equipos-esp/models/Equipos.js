const mongoose = require('mongoose');
const Schemma = mongoose.Schema;


const EquiposSchema = new Schemma({
    
    Placa_Inventario: {
        type: String,
        required: true
    },
    Nombre: {
        type: String,
        required: true
    },
    Descripcion: {
        type: String,
        required: true
    },
    Codigo_QR: {
        type: String,
        required: true
    },
    Ubicacion: {
        type: String,
        required: true
    },
    /*Puesto_Trabajo_Id: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'PuestoTrabajo',
        required: true
    },*/
}, {
        timestamps: false
    });


var Equipos = mongoose.model('Responsable', EquiposSchema);

module.exports = Equipos;
