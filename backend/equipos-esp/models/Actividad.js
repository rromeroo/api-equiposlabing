const mongoose = require('mongoose');
const Schemma = mongoose.Schema;


const ActividadSchema = new Schemma({

    Fecha_Prestamo:{
        type: String,
        required: true
    },
    Equipo_Asignado: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Equipos',
        required: true
    },
    Estado_Equipo: {
        type: String,
        required: true
    },
    
}, {
        timestamps: true
    });



var Actividades = mongoose.model('Actividad', ActividadSchema);

module.exports = Actividades;
